import numpy
import werkzeug
from flask import Flask
from flask_restplus import Api, Resource, fields
import cv2 as cv
import numpy as np

app = Flask(__name__)
api = Api(app, version='1.0', title='Board API')

ns = api.namespace('board', description='Board operations')
board = api.model('Board', {
    'ledsFailed': fields.Integer(),
    'linesFailed': fields.List(fields.String()),
    'columnsFailed': fields.List(fields.String()),

})

parser = api.parser()
parser.add_argument('img', type=werkzeug.FileStorage, required=True, location='files')

@ns.route('/')
class BoardResource(Resource):

    @api.doc(parser=parser)
    @api.marshal_with(board, code=201)
    def post(self):
        args = parser.parse_args()
        data = args['img']

        npimg = numpy.fromfile(data, numpy.uint8)
        # convert numpy array to image
        img = cv.imdecode(npimg, cv.IMREAD_GRAYSCALE)

        ret, img = cv.threshold(img, 220, 255, cv.THRESH_BINARY)

        img = cv.medianBlur(img, 5)
        cimg = cv.cvtColor(img, cv.COLOR_GRAY2BGR)

        circles = cv.HoughCircles(img,
                                  cv.HOUGH_GRADIENT,
                                  1,
                                  40,
                                  param1=1,
                                  param2=7,
                                  minRadius=10,
                                  maxRadius=30)
	
        ledsOn = 0
        lineslist = []
        columnslist = []

        if circles is not None:
            circles = np.uint16(np.around(circles))
            for i in circles[0, :]:
                cv.circle(cimg, (i[0], i[1]), i[2], (0, 255, 0), 5)

            ledsOn = len(circles[0])

            sortByLines = sorted(circles[0], key=lambda x: x[1], reverse=False)

            count = 0
            round = 1
            init = sortByLines[0][1]
            for passnum in range(len(sortByLines) - 1, 0, -1):
                for i in range(passnum):
                    if sortByLines[i][1] < init + 100:
                        count = count + 1
                    else:
                        round = round + 1
                        if count < 32:
                            line = str(9 - round)
                            lineslist.append(line)
                            print('Linha: ' + line)

                        count = 1
                        init = sortByLines[i + 1][1]

            sortByColumns = sorted(circles[0], key=lambda x: x[0], reverse=False)

            count = 0
            round = 0
            init = sortByColumns[0][0]
            for passnum in range(len(sortByColumns) - 1, 0, -1):
                for i in range(passnum):
                    if sortByColumns[i][0] < init + 100:
                        count = count + 1
                    else:
                        round = round + 1
                        if count < 8:
                            column = str(round)
                            columnslist.append(column)
                            print('Coluna: ' + column)

                        count = 1
                        init = sortByColumns[i + 1][0]

        ledsFailed = str(256 - ledsOn)

        return {
            'ledsFailed':ledsFailed,
            'linesFailed':lineslist,
            'columnsFailed':columnslist
        }

if __name__ == '__main__':
    app.run(host='192.168.0.22', debug=True)
    #app.run(debug=True)
